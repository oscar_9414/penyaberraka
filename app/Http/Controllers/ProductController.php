<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Cathegory;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth'); //aplicable a todos los métodos
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $products = Product::all();
        $products = Product::paginate(5);
        // dd($products);
        $categorias = Cathegory::all();
        return view('product.index', ['products' => $products],['categorias'=>$categorias]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias = Cathegory::all();
        // dd($categorias);
        return view ('product.create',['categorias'=>$categorias]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // echo "Creando";
        $reglas = [
            'name' => 'required|max:255|min:3',
            'price' => 'required|numeric',
            'cathegory_id' => 'min:1'
        ];
        $request->validate($reglas);
        $product = new Product();
        $product->fill($request->all());
        $product->save();
        return redirect('/products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);
        $categorias = Cathegory::all();
        return view('product.show',['product'=>$product],['categorias'=>$categorias]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $product = Product::findOrFail($id);
       $categorias = Cathegory::all();
       return view('product.edit', ['product' => $product],['categorias'=>$categorias]);
   }
    public function update(Request $request, $id)
    {
        $reglas = [
            'name' => 'required|max:255|min:3',
            'price' => 'required|numeric',
            'cathegory_id' => 'min:1'
        ];
        $request->validate($reglas);
        $product = Product::findOrFail($id);
        $product->fill($request->all());
        $product->save();
        return redirect('/products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::destroy($id);
        return back();
    }
}
