<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth'); //aplicable a todos los métodos
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $users = User::all();
        // return $users;
        $this->authorize('index', User::class);

        $users = User::paginate(5);

        return view('user.index', ['users' => $users]);


        //busca el fichero (uno de los dos):
        // /resources/views/user/index.php
        // /resources/views/user/index.blade.php
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validacion:
        $rules = [
            'name' => 'required|max:255|min:3',
            'email' => 'required|unique:users|max:255|email',
            'password' => 'required|max:255',
            // 'color' => 'required',
            // 'player' => 'required|max:255|min:5',
        ];

        $request->validate($rules);


        // $user = new User();
        // $user->name = $request->input('name');
        // $user->email = $request->input('email');
        // $user->password = bcrypt($request->input('password'));

        // $user->remember_token = str_random(10);
        // $user->save();

        // //opcion2
        // $user = User::create($request->all());

        //opcion3
        $user = new User();
        $user->fill($request->all());
        $user->password = bcrypt($user->password);
        $user->save();

        return redirect('/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        // $user = User::find($id);
        // if ($user == null) {
        //     abort(404, 'Prohibido!!!!');
        //     // response()->view('errors.404', [], 404);
        // }
        // $user = User::findOrFail($id);
        return view('user.show', [
            'user' => $user,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $this->authorize('update',$user);//existe un bobjeto a actualizar
        return view('user.edit', ['user' => $user]);

        // para create/store n hay u usuario
        // $this->authorize('update',User::class);//hayq ue pasarle el nombre de la clase

    }
    public function edit2($id)
    {
        // $this->authorize('update',$user);//existe un bobjeto a actualizar
        // return view('user.edit', ['user' => $user]);

        $curretUser = \Auth::User();
        $user = User::findOrFail($id);
        // dd($user);
        // echo "Edit 2";

        if ($curretUser->can('update',$user)) {
            return "Se puede editar este usuario oooooooleeeeeeeeeeee";
        } else {
            return back();
            return "No seas mamon este usuario no es tu usuario";
        }

        // para create/store n hay u usuario
        // $this->authorize('update',User::class);//hayq ue pasarle el nombre de la clase

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rules = [
            'name' => 'required|max:255|min:4',
            'email' => "required|unique:users,email,$id,id|max:255|email",
        ];

        $request->validate($rules);




        $user = User::findOrFail($id);
        $user->fill($request->all());

        $this->authorize('update', $user);


        $user->save();


        return redirect('/users/' . $user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $user = User::findOrFail($id);
        // $user->delete();

        // $curretUser = \Auth::User();
        $user = User::findOrFail($id);
      //   if ($user->can('delete', $user)) {
      //     echo "se puede borrar:";
      // } else {
      //     echo 'No autorizado.';
      // }
        User::destroy($id);
        $this->au0thorize('delete',$user);
        return back();
  }

  public function especial()
  {
    $users = User::where('id', '>=', 15)
    ->where('id', '<=', 20)
    ->get();

    dd($users);
    return "especial";
    return redirect('/users');
        // return "Especial";
}
}
