<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cathegory;
use App\Product;

class CathegoryController extends Controller
{
    public function __construct()
    {
      // $this->middleware('auth'); //aplicable a todos los métodos
      // $this->middleware('log')->only('index');//solamente a ....
      $this->middleware('auth')->except('index');//todos excepto ...
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias = Cathegory::paginate(5);
        return view('category.index',['categorias'=>$categorias]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reglas = [
            'name' => 'required|max:255|min:3'
        ];
        $request->validate($reglas);
         $category = new Cathegory();
         $category->fill($request->all());
         $category->save();
         return redirect('/category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $productos = Product::all();
        $categoria = Cathegory::findOrFail($id);
        // return $productos;
        // echo "Mostrando". $categoria->name;
        // dd($productos);
        return view('category.show', ['categoria' => $categoria],['productos' => $productos]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categoria = Cathegory::findOrFail($id);
        return view('category.edit',['categoria'=>$categoria]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $reglas = [
            'name'=> 'required|max:255|min:3'
        ];
        $request->validate($reglas);
        $categoria = Cathegory::findOrFail($id);
        $categoria->fill($request->all());
        $categoria->save();
        return redirect('/category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cathegory::destroy($id);
        return back();
    }
}
