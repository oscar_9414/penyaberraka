<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cathegory extends Model
{
    protected $fillable = [
        'name'
    ];
    public function role()
    {
       return $this->belongsTo(\App\Role::class);
       //belongsTo
       //hasMany
       //hasOne
       //belongsToMany (En los dos)
    }
}
