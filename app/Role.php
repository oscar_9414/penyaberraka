<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function users()
    {
        return $this->hasMany(User::class);
        //belongsTo
        //HasMany
        // HasOne
        // belongsToMany
        // Ejemplo N:M productos con categorias
        // La tabla de la relación se debe llamar obligatoriamente cathegory_product
        // alfabeticamente, cathegory antes que product.
        // product cathegory no funcionaria

    }
}
