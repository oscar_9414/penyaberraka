@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-body">
                    <h1>Datos de la categoría. {{ $categoria->name }}</h1>
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <td>Nombre</td>
                                <td>ID</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ $categoria->name }}</td>
                                <td>{{ $categoria->id }}</td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: center;"><h3>Productos de esta categoría</h3></td>
                            </tr>
                            <tr>
                                <td>Nombre</td>
                                <td>Precio</td>
                            </tr>
                            <?php $cont = 0; ?>
                            @foreach($productos as $producto)
                            @if($producto->cathegory_id == $categoria->id)
                            <?php $cont++; ?>
                            <tr>
                                <td>{{$producto->name}}</td>
                                <td>{{$producto->price}}</td>
                            </tr>
                            @endif
                            @endforeach
                            @if($cont == 0)
                            <tr>
                                <td colspan="2" style="text-align:  center;"><h2>No hay productos</h2></td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                    <a href="/category" class="btn btn-primary">Volver</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
