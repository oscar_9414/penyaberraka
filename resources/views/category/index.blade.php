@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header"><h1>lista de las categorías</h1><br>
          <a href="/category/create" class="btn btn-primary">crear nueva categoría</a>
          <div class="card-body">
            <table class="table table-striped table-hover">
              <thead>
                <tr>
                  <td><h4> Nombre </h4></td>
                  <td><h4>id</h4></td>
                  <td><h4>Acciones</h4></td>
                </tr>
              </thead>
              <tbody>
                @forelse ($categorias as $categoria)
                <tr>
                 <td>{{ $categoria->name}}</td>
                 <td>{{$categoria->id}}</td>
                 <td>
                   <a href="/category/{{ $categoria->id }}/edit" class="btn btn-primary">Editar</a>
                   <a href="/category/{{ $categoria->id }}" class="btn btn-primary">Ver</a>
                   <form method="post" action="/category/{{ $categoria->id }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="submit" value="borrar">
                  </form>
                </td>
              </tr>

              @empty
              <h1>No hay categorias</h1>
              @endforelse
            </tbody>
          </table>
          {{ $categorias->render() }}
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
