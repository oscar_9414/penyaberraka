@extends('layouts.app')

@section('title', 'Usuarios')

@section('content')

    <h1>
        Este es el detalle del Role <?php echo $role->id ?>
    </h1>

    <ul>
        <li>Nombre: <strong>{{ $role->name }}</strong></li>
    </ul>

    <h2>Lista de usuarios</h2>
    <table class="table">
        <tr>
            <td><h3>Name</h3></td>
            <td><h3>Email</h3></td>
        </tr>
        @foreach ($role->users as $user)
        <tr>
            <td>{{ $user->name}}</td>
            <td>{{$user->email}}</td>

        </tr>

        @endforeach
    </table>



@endsection

